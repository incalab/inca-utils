# Adding Fontawesome to your project

## Overview

An icons library facilitates the use of icons in your project. This guide explains how to install and use the [Fontawesome](https://fontawesome.com/) library.

## Installation

Install the free package with:

```sh
npm install --save @fortawesome/fontawesome-free
```

## Usage

First import the library in a root file of your project:

::: code-group

```ts [Svelte]
// src/main.[js,ts]
import '@fortawesome/fontawesome-free/js/all.min.js';
```

```svelte [SvelteKit]
<!-- src/routes/+layout.svelte -->
<script>
  import '@fortawesome/fontawesome-free/js/all.min.js';
</script>
```
:::

Now you can start using the icons in any part of your project:

```svelte
<i class="fa-solid fa-user"></i>
```

Check out all the available icons in the [Fontawesome site](https://fontawesome.com/search?o=r&m=free).