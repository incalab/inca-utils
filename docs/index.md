---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "InCA Utils"
  text: "Collection of utilities for InCA projects"
  tagline: Collection of components and APIs ready to use.
  actions:
    - theme: brand
      text: Getting Started
      link: /getting-started
    - theme: alt
      text: NPM package
      link: https://www.npmjs.com/package/inca-utils
    - theme: alt
      text: View on GitLab
      link: https://gitlab.com/shockerqt/inca-utils

# features:
#   - title: Feature A
#     details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
#   - title: Feature B
#     details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
#   - title: Feature C
#     details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
# ---

