import { defineConfig } from 'vitepress';

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: 'InCA Utils Docs',
  description: 'Documentation for InCA Utils package',
  outDir: '../public',
  base: '/inca-utils/',
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Examples', link: '/markdown-examples' },
    ],

    sidebar: [
      {
        text: 'Guide',
        items: [{ text: 'Getting Started', link: '/getting-started' },
      {text: 'Demos', link:'/demos'}],
      },
      {
        text: 'Components',
        items: [
          {
            text: 'Main components',
            items: [
              { text: 'Tiles', link: '/components/tiles' },
              { text: 'Trainer Button', link: '/components/trainer-button' },
              { text: 'Action Button', link: '/components/action-button' },
            ],
          },
          {
            text: 'Utility Components',
            items: [
              { text: 'ThemePicker', link: '/components/theme-picker' },
              { text: 'Fa', link: '/components/fa' },
              { text: 'NumberInput', link: '/components/number-input' },
            ],
          },
        ],
      },
      {
        text: 'APIs',
        items: [
          { text: 'Theme', link: '/apis/theme' },
          { text: 'Speech', link: '/apis/speech' },
          { text: 'Fullscreen', link: '/apis/fullscreen' },
        ],
      }
    ],

    socialLinks: [
      {
        icon: {
          svg: `
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 256"><path d="M231.9,169.8l-94.8,65.6a15.7,15.7,0,0,1-18.2,0L24.1,169.8a16.1,16.1,0,0,1-6.4-17.3L45,50a12,12,0,0,1,22.9-1.1L88.5,104h79l20.6-55.1A12,12,0,0,1,211,50l27.3,102.5A16.1,16.1,0,0,1,231.9,169.8Z"/></svg>
      `,
        },
        link: 'https://gitlab.com/shockerqt/inca-utils',
      },
    ],
  },
});
