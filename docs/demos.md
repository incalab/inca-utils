# Demos

## Examples

You can check here every functionality of the library in action.

[Go to examples](pathname://demo/index.html)

[Check the source code](https://gitlab.com/shockerqt/inca-utils/-/tree/main/example)


## Complete App

InCA CompareFast is an application on which the subjects have to choose the max quantity between a set of tiles. The implementation is based on InCA Utils.

[Go to application](https://buho.dcc.uchile.cl/~fjana/)

[Check the source code](https://gitlab.com/shockerqt/inca-comparefast)
