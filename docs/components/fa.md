# Fa

## Overview
A simple component to render FontAwesome icons.

## Usage

```svelte
<script lang="ts">
  // Import the component
  import { Fa } from 'inca-utils';

  // Import some icons
  import { faStar, faHeart } from '@fortawesome/free-solid-svg-icons';
</script>

<Fa icon={faStar} />
<Fa icon={faHeart} size={1.5} />
```

## Search Icons

Check out all the available icons in the [Fontawesome site](https://fontawesome.com/search?o=r&m=free).

## Source

[View source code on GitLab](https://gitlab.com/shockerqt/inca-utils/-/blob/main/lib/components/Fa.svelte)