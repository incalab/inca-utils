# Tiles

## Overview

Tiles are representations of discrete or continous quantities.

## Usage

```svelte
<script lang="ts">
  import { Tile } from 'inca-utils';
</script>

<!-- Represents a heap mode tile with a 4x4 grid -->
<Tile on:click={() => alert('click')} mode="heap" gridSize={4} quantity={11} />

<!--
  No props are required,
  this will render a Tile with default props.
-->
<Tile />


<!-- Customize the component -->
<Tile
  --width="96px"
  --border-radius="40px"
  --border-size="5px"
  --border-color="blue"
  --background-color="red"
  --foreground-color="#ccc"
/>
```

## Demo

<iframe
  style="
  width: 100%;
  height: 500px;
  border: none;
  border-radius: 8px;
"
  src="https://stackblitz.com/edit/inca-utils-tile-example?ctl=1&embed=1&file=src%2FApp.svelte&hideExplorer=1&hideNavigation=1"
/>

::: info TIP
Try changing the props.
:::

## Props

| Prop name  | Type       | Default  | Description  |
| ---------- | ---------- | -------- | ------------------------------------------------------------------- |
| `on:click`   | `function` | `undefined` | Trigger a click event. |
| `mode`     | `Mode`     | `"dice"` | Representation mode for the tile. Check the [type declarations](#type-declarations) to view `Mode` type in details.                                                                                                                    |
| `gridSize` | `number`   | `3`      | The gridSize represents the number or rows and columns that a tile can have on grid like modes (_like `"dice"` or `"heap"` modes_).                                                                                                    |
| `quantity` | `number`   | `6`      | Represents the quantity for each mode. Modes can have different representation of quantities like the number of dots on `"dice"` mode or the radius of the circle on `"disc"` mode. The quantity is limited by `gridSize`<sup>2</sup>. |


## Styling

You can customize the component using these props:
* `--background-color` Set the background color
* `--foreground-color` Set the color of the quantity representations
* `--width` Set the total width of the tile
* `--border-radius` Set the border radius
* `--border-size` Set the border size
* `--border-color` Set the border color

## Exported Types
::: code-group
```ts [Tile.svelte]
export type Mode =
  | 'dice'
  | 'rect'
  | 'disc'
  | 'heap'
  | 'stack'
  | 'grid'
  | 'donut';
```
:::

## Source

[View source code on GitLab](https://gitlab.com/shockerqt/inca-utils/-/blob/main/lib/components/Tile.svelte)
