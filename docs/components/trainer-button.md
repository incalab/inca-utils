# Trainer Button

## Overview

Trainer buttons are intended to be used only by humans trainers, it add a layer of difficulty (e.g. longpress) in order to avoid the non-human subject to interact with it.

### Use Cases

- Buttons to navigate to views not allowed to the subject (e.g. settings view, about view).
- A button to toggle fullscreen.

## Usage

```svelte
<script lang="ts">
  import { TrainerButton, Fa } from 'inca-utils';
  import {
    faExpand,
    faGear,
    faArrowLeft,
  } from '@fortawesome/free-solid-svg-icons';
</script>

<!-- Empty Trainer Button, default props -->
<TrainerButton />

<!-- Trainer Button with default longpress time -->
<TrainerButton
  label="Longpress default"
  on:click={() => console.log('Trainer button 1 pressed')}
>
  <!-- Check Fa section for details of this component -->
  <Fa icon={faExpand} />
</TrainerButton>

<!-- Trainer button with longpress time of 0.5 seconds -->
<TrainerButton
  label="Longpress 0.5s"
  longpressTime={500}
  on:click={() => console.log('Trainer button 2 pressed')}
>
  <Fa icon={faGear} />
</TrainerButton>


<!-- Trainer button with no longpress -->
<TrainerButton
  label="No longpress"
  longpressTime={0}
  on:click={() => console.log('Trainer button 3 pressed')}
>
  <Fa icon={faArrowLeft} />
</TrainerButton>


<!-- Style the button -->
<TrainerButton
  --font-size="0.6em"
  --width="32px"
  --background-color="green"
  --color="black"
/>
```

## Props

| Prop name  | Type       | Default  | Description  |
| ---------- | ---------- | -------- | --------------- |
| `on:click`   | `function` | `undefined` | Trigger a click event. |
| `longpressTime` | `number` | `1000` | Longpress time in milliseconds. |
| `label` | `string` | `''` | Text displayed below the button. |

## Slots
Passing a child to the component will render it inside the button.

## Styling
You can customize the component using these props:
* `--background-color` Set the background color
* `--color` Set the color for the label
* `--width` Set the total width of the tile
* `--font-size` Set the font size of the label


## Source

[View source code on GitLab](https://gitlab.com/shockerqt/inca-utils/-/blob/main/lib/components/TrainerButton.svelte)
