# Getting Started

## Overview

InCA Utils is a collections of components and functionalities shared by projects of InCA, this documentation presents basic example usages of each functionality.

Check the demos page for a preview of all the functionalitites.

## Installation

### Prerequisites

- [Node.js](https://nodejs.org/) with [npm](https://www.npmjs.com/)
- A [Svelte](https://svelte.dev/) or [SvelteKit](https://kit.svelte.dev/) project

You can install InCA Utils in your project with:

::: code-group

```sh [npm]
$ npm i inca-utils
```

:::


### Quick Start

::: code-group
```svelte [App.svelte]
<script lang="ts">
  // Inject library styles
  import 'inca-utils/styles.css'

  // Import from library
  import {
    Tile,
    ActionButton,
    TrainerButton,
    ThemePicker,
    Fa,
    fullscreen,
  } from 'inca-utils';

  // Icons from fontawesome (included in the library)
  import { faExpand } from '@fortawesome/free-solid-svg-icons';
</script>

<!-- Add components to your views -->
<main class="app">
  <Tile />

  <ActionButton mode="ready" />
  <ActionButton mode="exit" />
  
  <TrainerButton on:click={fullscreen} label="Fullscreen">
    <Fa icon={faExpand} />
  </TrainerButton>
  
  <ThemePicker />
</main>

<style>
  main.app {
    background-color: var(--inca-background);
    color: var(--inca-foreground);
  }
</style>
```
:::

## Source

[View source code on GitLab](https://gitlab.com/shockerqt/inca-utils/)
