# Speech

## Overview

The Speech API let you play voices and save to play anywhere. It is based on the [Web Speech API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Speech_API)


## Usage

### Save some voices

```svelte
  <!-- Settings.svelte -->
<script>
  import { speech } from 'inca-utils/api';

  // save a text to speech later
  $speech.speeches['ready voice'] = 'Are your ready?'
</script>

<!-- Save using input binds -->
<input
  bind:value={$speech.speeches['good boy']}
  type="text"
/>
```

### Play them in your game
```svelte
<!-- Game.svelte -->
<script>
  import { speech } from 'inca-utils';

  // speak output: 'Are you ready?'
  speech.play('ready voice'); 

  // the browser will say whatever the user wrote on the input
  speech.play('good boy');
</script>
```

### Use arguments

```svelte
<script>
  import { speech } from 'inca-utils';

  $speech.speeches['levelNumber'] = 'The current level is {level}'
  
  // expected output: 'The current level is  2534'
  speech.play('levelNumber', { level: 2534 })
</script>
```

## Configuration

TODO

Check [ComperaFast](https://gitlab.com/shockerqt/inca-comparefast/-/blob/master/src/components/SettingsSpeechSection.svelte)
or the [Example App](https://gitlab.com/shockerqt/inca-utils/-/blob/main/example/src/components/SpeechSection.svelte) for reference.

## Source

[View source code on GitLab](https://gitlab.com/shockerqt/inca-utils/-/blob/main/lib/apis/theme.ts)