# Theme

## Overview

The Theme API is intended to control common colors and styles of the whole application. All components of this library are styled using this API.

The API allows to easily use and change the theme in your project, storing the preferences on the `LocalStorage`.

## Use the API colors on your project

### With css

```svelte

<div class="background" />

<style>
  backgroud-color: var(--inca-background);
</style>

```

### With javascript

```svelte
<script lang="ts">
  // import the theme store
  import { theme } from 'inca-utils/api';
</script>

<div
  style:background-color={$theme.colors.background}
/>
```

## Color list

::: details CSS variables
```
--inca-red
--inca-on-red
--inca-green
--inca-on-green
--inca-yellow
--inca-on-yellow
--inca-primary
--inca-on-primary
--inca-primary-container
--inca-on-primary-container
--inca-secondary
--inca-on-secondary
--inca-secondary-container
--inca-on-secondary-container
--inca-tertiary
--inca-on-tertiary
--inca-tertiary-container
--inca-on-tertiary-container
--inca-error
--inca-on-error
--inca-error-container
--inca-on-error-container
--inca-background
--inca-on-background
--inca-surface
--inca-on-surface
--inca-surface-variant
--inca-on-surface-variant
--inca-outline
--inca-outline-variant
--inca-shadow
--inca-scrim
--inca-inverse-surface
--inca-inverse-on-surface
--inca-inverse-primary
--inca-success
--inca-success-container
--inca-on-success
--inca-on-success-container
--inca-game-background
--inca-game-foreground
--inca-tile-background
--inca-tile-foreground
--inca-tile-border
```
:::

::: details js Variables

```
$theme.colors.red
$theme.colors.onRed
$theme.colors.green
$theme.colors.onGreen
$theme.colors.yellow
$theme.colors.onYellow
$theme.colors.primary
$theme.colors.onPrimary
$theme.colors.primaryContainer
$theme.colors.onPrimaryContainer
$theme.colors.secondary
$theme.colors.onSecondary
$theme.colors.secondaryContainer
$theme.colors.onSecondaryContainer
$theme.colors.tertiary
$theme.colors.onTertiary
$theme.colors.tertiaryContainer
$theme.colors.onTertiaryContainer
$theme.colors.error
$theme.colors.onError
$theme.colors.errorContainer
$theme.colors.onErrorContainer
$theme.colors.background
$theme.colors.onBackground
$theme.colors.surface
$theme.colors.onSurface
$theme.colors.surfaceVariant
$theme.colors.onSurfaceVariant
$theme.colors.outline
$theme.colors.outlineVariant
$theme.colors.shadow
$theme.colors.scrim
$theme.colors.inverseSurface
$theme.colors.inverseOnSurface
$theme.colors.inversePrimary
$theme.colors.success
$theme.colors.successContainer
$theme.colors.onSuccess
$theme.colors.onSuccessContainer
$theme.colors.gameBackground
$theme.colors.gameForeground
$theme.colors.tileBackground
$theme.colors.tileForeground
$theme.colors.tileBorder 
```

:::

## Source

[View source code on GitLab](https://gitlab.com/shockerqt/inca-utils/-/blob/main/lib/apis/theme.ts)
