# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## [v1.2.3]

### Fixed

- Typo on support version

## [v1.2.1]

### Added

- Support for svelte@4

## [v1.2.0]

### Added

- Styling props to docs

### Changed

- New prop --width replacing size in most of the components

### Fixed

- Typo on docs/action-button.md

## [v1.1.3]

### Changed

- package.json structure

## [v1.1.2]

### Fixed

- scss lang to TrainerButton styles

### Changed

- package.json structure

## [v1.1.1]

### Added

- Import for apis on `'inca-utils/api'`

## [v1.1.0]

### Added

- Fallback for css vars

### Changed

- lazy import to DOM APIs

## [v1.0.3]

### Changed

- Added `user-select: none` and `cursor: pointer` to buttons

### Fixed

- Addressed circular dependencies

## [v1.0.2]

### Removed

- Removed JSON resolvers dependencies

## [v1.0.1]

### Added

- Simple input:disabled indicator

## [v1.0.0]

### Changed

- Refactor all the components to use only the CSS variables
- Now the CSS styleSheets need to be imported on the projects to use
- Examples update
- Docs update

## [v0.4.1]

### Added

- New functionality that autoinject the colors variables using CSSStyleSheet API
- A new suite of styles in sass

### Changed

- The examples are updated to use the new styles

### Fixed

- A bug the was not allowing to change the tileBackground advanced color

### Removed

- setCss function is no longer needed

## [v0.3.1]

### Removed

- SASS dependencies

## [v0.3.0]

### Added

- Speech API

### Changed

### Fixed

- Change references of Tile.svelte to new specific colors
- Minor visual glitch with borderless rounded tiles

## [v0.2.0]

### Added

- Generic template of CONTRIBUTING.md
- Examples of action buttons on example app

### Changed

- Changed the longpress transition to indicate that it is a lonpress button
- Refactor of ThemePicker component

### Fixed

- Center icon on TrainerButton
- Change input font color from forms.css
- Minor bugs on NumberInput component

### Removed

- TilePreview
- TileProps interface

## [0.1.1] - 2023-06-01

### Added

- Utility component to display FontAwesome icons
- ActionButton component
- CHANGELOG.md

### Changed

- Tile component and TrainerButton component now use event dispatcher to handle actions

### Fixed

## [0.1.0] - 2023-05-31

### Added

- TrainerButton component
- ThemePicker component
- TilePreview component

### Changed

- Theme API now use Material Design v3 colors
- Example App update with current changes
