## Get started

Checkout the [documentation](https://shockerqt.gitlab.io/inca-utils/) and the [demo page](https://shockerqt.gitlab.io/inca-utils/demo/index).

## Changelog

[CHANGELOG.md](/CHANGELOG.md)

## To Do

- Add documentation for ThemePicker and NumberInput
- Algorithm to extend dice mode
- Add Tile snake mode
