import { defineConfig } from 'vite';
import { svelte } from '@sveltejs/vite-plugin-svelte';

// https://vitejs.dev/config/
export default defineConfig({
  base: '/inca-utils/demo/',
  build: {
    outDir: '../docs/public/demo',
    emptyOutDir: true
  },
  server: {
    base: '/',
    fs: {
      allow: ['..', '../../..']
    }
  },
  plugins: [svelte()]
});
