// import rawThemes from '../data/themes.json';
import { writable } from 'svelte/store';
import { schemesFromColor } from 'material-color-scheme';
import type {
  CssMaterialColors,
  MaterialColors
} from 'material-color-scheme/dist/theme';

export interface IncaColors extends MaterialColors, AdvancedColors {
  red: string;
  onRed: string;
  green: string;
  onGreen: string;
  yellow: string;
  onYellow: string;
}

export interface Theme {
  name: string;
  options: {
    source: string;
    darkMode: boolean;
    roundedTile: boolean;
    smallBorderTile: boolean;
    highContrastTile: boolean;
  };
  colors?: IncaColors;
  css?: CssMaterialColors;
  original?: {
    colors?: IncaColors;
    css?: CssMaterialColors;
  };
}

export interface ThemeCollection {
  [key: string]: Theme;
}

export interface RawTheme {
  source: string;
  darkMode?: boolean;
  roundedTile?: boolean;
  smallBorderTile?: boolean;
  highContrastTile?: boolean;
}

export interface IncaScheme {
  dark: IncaColors;
  light: IncaColors;
  cssDark: CssMaterialColors;
  cssLight: CssMaterialColors;
}

export interface AdvancedColors {
  gameBackground?: string;
  gameForeground?: string;
  tileBackground?: string;
  tileForeground?: string;
  tileBorder?: string;
}

const rawThemes = {
  default: {
    source: '#065f46',
    darkMode: false,
    highContrastTile: true,
    roundedTile: false,
    smallBorderTile: false
  },
  dark: {
    source: '#bac3ff',
    darkMode: true,
    highContrastTile: true
  },
  comparefast: {
    source: '#20648a'
  },
  whatismore: {
    source: '#00ff00',
    roundedTile: true,
    highContrastTile: true,
    smallBorderTile: true
  }
};

const fromStorage = (key: string) => {
  return (
    typeof window !== 'undefined' && localStorage && localStorage.getItem(key)
  );
};

const toStorage = (key: string, value: string) => {
  return (
    typeof window !== 'undefined' &&
    localStorage &&
    localStorage.setItem(key, value)
  );
};

const themeAddAdvancedColors = (theme: Theme, advanced?: AdvancedColors) => {
  const colors = {
    ...theme.colors,
    gameBackground: advanced?.gameBackground ?? theme.colors.background,
    gameForeground: advanced?.gameForeground ?? theme.colors.onBackground,
    tileBackground:
      advanced?.tileBackground ??
      (theme.options.highContrastTile
        ? theme.colors.surfaceVariant
        : theme.colors.primaryContainer),
    tileForeground: advanced?.tileForeground ?? theme.colors.primary,
    tileBorder: advanced?.tileBorder ?? theme.colors.onBackground
  };

  const css = {
    ...theme.css,
    '--inca-game-background': colors.gameBackground,
    '--inca-game-foreground': colors.gameForeground,
    '--inca-tile-background': colors.tileBackground,
    '--inca-tile-foreground': colors.tileForeground,
    '--inca-tile-border-color': colors.tileBorder,
    '--inca-tile-border-radius': theme.options.roundedTile ? '0.8em' : '0',
    '--inca-tile-border-size': theme.options.smallBorderTile ? '0.2em' : '0.4em'
  };
  return {
    ...theme,
    colors,
    css
  };
};

const themeFromRaw = (raw: RawTheme, name: string): Theme => {
  const addedColors = {
    red: '#ba1a1a',
    onRed: '#410002',
    green: '#026e00',
    onGreen: '#002200',
    yellow: '#e3bc1e',
    onYellow: '#494122'
  };

  const schemes = schemesFromColor(
    raw.source,
    'inca',
    [{ name: 'success', value: '#00ff00', blend: false }],
    addedColors
  ) as IncaScheme;

  const mode = raw.darkMode ? 'dark' : 'light';
  const cssMode = raw.darkMode ? 'cssDark' : 'cssLight';

  const theme = themeAddAdvancedColors({
    name,
    options: {
      source: raw.source,
      darkMode: raw.darkMode ?? true,
      roundedTile: !!raw.roundedTile,
      smallBorderTile: !!raw.smallBorderTile,
      highContrastTile: !!raw.highContrastTile
    },
    colors: schemes[mode],
    css: schemes[cssMode]
  });

  return {
    ...theme,
    original: {
      colors: theme.colors,
      css: theme.css
    }
  };
};

const generateRules = (styles: CssMaterialColors) => {
  if (
    typeof window !== undefined &&
    typeof document !== undefined &&
    typeof CSSStyleSheet !== undefined
  ) {
    const styleSheet = new CSSStyleSheet();
    document.adoptedStyleSheets = [styleSheet];
    let rules = ':root {';
    Object.entries(styles).forEach(([key, value]) => {
      rules += `${key}:${value};`;
    });
    rules += '}';
    styleSheet.replace(rules);
  }
};

const themeCollectionStore = () => {
  const localStorageKey = 'inca-custom-theme';
  const customThemeSerialized = fromStorage(localStorageKey);
  const customTheme = customThemeSerialized
    ? themeFromRaw(JSON.parse(customThemeSerialized), 'custom')
    : themeFromRaw(rawThemes.default, 'custom');
  const themes = Object.entries(rawThemes).reduce(
    (prev, [name, value]) => ({ ...prev, [name]: themeFromRaw(value, name) }),
    {}
  );

  const { subscribe, update } = writable<ThemeCollection>({
    ...themes,
    custom: customTheme
  });

  return {
    subscribe,
    all() {
      let themeCollection: ThemeCollection;
      const unsuscribe = subscribe((_themeCollection) => {
        themeCollection = _themeCollection;
      });
      unsuscribe();
      return themeCollection;
    },
    updateCustom(partialRaw: Partial<RawTheme>) {
      update((themes) => {
        const raw = {
          ...themes.custom.options,
          ...partialRaw
        };
        toStorage(localStorageKey, JSON.stringify(raw));
        return { ...themes, custom: themeFromRaw(raw, 'custom') };
      });
    }
  };
};

const advancedColorsStore = () => {
  const localStorageKey = 'inca-advanced-colors';
  const advancedColorsSerialized = fromStorage(localStorageKey);
  const advancedColors = JSON.parse(advancedColorsSerialized) || {};
  let timeout: number;

  const { subscribe, update, set } = writable<AdvancedColors>(advancedColors);

  subscribe((advancedColors) => {
    if (timeout) clearTimeout(timeout);

    timeout = setTimeout(() => {
      toStorage(localStorageKey, JSON.stringify(advancedColors));
    }, 1000);
  });

  return {
    subscribe,
    update,
    set
  };
};

const themeStore = (
  themesStore: ReturnType<typeof themeCollectionStore>,
  advancedColors: ReturnType<typeof advancedColorsStore>
) => {
  const localStorageKey = 'inca-theme';
  let currentTheme = fromStorage(localStorageKey) || 'default';
  let timeout: number;
  let collection: ThemeCollection;
  let advanced: AdvancedColors;

  themesStore.subscribe((themeCollection) => {
    collection = themeCollection;
  })();

  const { subscribe, set, update } = writable<Theme>(collection[currentTheme]);

  const setTheme = (theme: string | Theme) => {
    let name: string;
    if (typeof theme === 'string') name = theme;
    else if (typeof theme === 'object' && 'name' in theme) ({ name } = theme);

    if (Object.keys(themesStore.all()).includes(name)) {
      if (name === 'custom') {
        set(themeAddAdvancedColors(collection[name], advanced));
      } else set({ ...collection[name] });
    }
  };

  advancedColors.subscribe((advancedColors) => {
    advanced = advancedColors;
    setTheme(currentTheme);
  });

  themesStore.subscribe((themeCollection) => {
    collection = themeCollection;
    setTheme(currentTheme);
  });

  subscribe((theme) => {
    currentTheme = theme.name;

    // sync CSS
    generateRules(theme.css);
    if (timeout) clearTimeout(timeout);

    timeout = setTimeout(() => {
      toStorage(localStorageKey, theme.name);
    }, 1000);
  });

  return {
    subscribe,
    update,
    set: setTheme
  };
};

export const themeCollection = themeCollectionStore();
export const advancedColors = advancedColorsStore();
export const theme: ThemeStore = themeStore(themeCollection, advancedColors);

export type ThemeStore = ReturnType<typeof themeStore>;
export type ThemeCollectionStore = ReturnType<typeof themeCollectionStore>;
export type AdvancedColorsStore = ReturnType<typeof advancedColorsStore>;
