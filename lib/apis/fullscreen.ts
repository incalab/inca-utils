export const fullscreen = () => {
  if (document?.body) {
    if (!document.fullscreenElement) {
      document.body.requestFullscreen();
      screen?.orientation?.lock('landscape-primary');
    } else {
      document.exitFullscreen();
      screen?.orientation?.unlock();
    }
  }
};
