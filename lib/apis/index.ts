// APIs
export { themeCollection, advancedColors, theme } from './theme';
export { speech } from './speech';
export { fullscreen } from './fullscreen';
