import { writable } from 'svelte/store';

interface SpeechConfig {
  lang: string;
  pitch: number;
  volume: number;
  rate: number;
  voiceName: string;
}

interface Speeches {
  [key: string]: string;
}

interface SpeechStore {
  config: SpeechConfig;
  speeches: Speeches;
  voices: string[];
}

const defaultConfig: SpeechConfig = {
  lang: 'en-US',
  pitch: 1,
  volume: 1,
  rate: 1,
  voiceName: 'default'
};

const template = (string: string, obj: { [key: string]: string }) => {
  let result = string;
  Object.entries(obj).forEach(([key, value]) => {
    result = result.replace(new RegExp(`{${key}}`, 'g'), value);
  });
  return result;
};

const createSpeechStore = () => {
  const synth = window.speechSynthesis;
  const localStorageKey = 'inca-speech';
  const saveDelay = 1000;
  const storedData = window.localStorage?.getItem(localStorageKey);
  const parsedData = storedData && JSON.parse(storedData);
  const data: SpeechStore = {
    config: parsedData?.config || defaultConfig,
    speeches: parsedData?.speeches || {},
    voices: []
  };

  let timeout: number;

  const { subscribe, update, set } = writable<SpeechStore>(data);

  synth.onvoiceschanged = () => {
    update((speechStore) => ({
      ...speechStore,
      voices: synth.getVoices().map((voice) => voice.name)
    }));
  };

  subscribe((data) => {
    if (timeout) clearTimeout(timeout);

    timeout = setTimeout(() => {
      window.localStorage?.setItem(localStorageKey, JSON.stringify(data));
    }, saveDelay);
  });

  return {
    subscribe,
    set,
    update,
    /**
     * If there is a saved speech with it name, play it
     * @param name the name of the saved speech
     */
    play(name: string, params?: { [key: string]: string }) {
      const unsuscribe = subscribe(({ speeches, config }) => {
        synth.cancel();

        let text = speeches[name];
        if (!text) return;
        if (params) text = template(text, params);

        const utterThis = new SpeechSynthesisUtterance(text);

        if (config.voiceName !== 'default') {
          for (const voice of synth.getVoices()) {
            if (voice.name === config.voiceName) utterThis.voice = voice;
          }
        }

        utterThis.lang = config.lang;
        utterThis.pitch = config.pitch;
        utterThis.rate = config.rate;
        utterThis.volume = config.volume;
        synth.speak(utterThis);
      });

      unsuscribe();
    }
  };
};

export const speech = createSpeechStore();
